FROM ubuntu

RUN apt-get update && apt-get install -y ansible sudo git
RUN echo 'alias test="ansible-playbook local.yml && su - jonas"' >> /root/.bashrc

RUN useradd -ms /bin/bash jonas
