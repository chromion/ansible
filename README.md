# Dev Env

ansible config to install my dev environment

## prerequisites:
- curl


## install:
```
sh -c "$(curl -fsSL https://gitlab.com/chromion/ansible/-/raw/main/ansible-run)"
```
